from occam.log     import loggable
from occam.manager import manager

@loggable
@manager("foo")
class FooManager:
  def __init__(self):
    """ Initializes the manager.
    """
